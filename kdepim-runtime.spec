%undefine __cmake_in_source_build
# uncomment to enable bootstrap mode
#global bootstrap 1


%if !0%{?bootstrap}
## skipping tests, seem to hang indefinitely starting with 18.04.0
#global tests 1
%endif

%global framework kdepim-runtime
%define orig_name kdepim-runtime

Name:    kdepim-runtime-etesync
Summary: KDE PIM Runtime Environment
Epoch:   1
Version: 23.04.1
Release: 1%{?dist}

License: GPLv2
URL:     https://invent.kde.org/pim/%{orig_name}.git

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0: https://download.kde.org/%{stable}/release-service/%{version}/src/%{orig_name}-%{version}.tar.xz

AutoReq:       no
AutoReqProv:   no

# handled by qt5-srpm-macros, which defines %%qt5_qtwebengine_arches
%{?qt5_qtwebengine_arches:ExclusiveArch: %{qt5_qtwebengine_arches}}

## upstream patches

# We need libetebase
Requires: libetebase
BuildRequires: libetebase

Requires:       %{orig_name}

BuildRequires:  desktop-file-utils
BuildRequires:  extra-cmake-modules
BuildRequires:  gettext
BuildRequires:  kf5-rpm-macros

BuildRequires:  cmake(Grantlee5)
BuildRequires:  cmake(KF5Codecs)
BuildRequires:  cmake(KF5Config)
BuildRequires:  cmake(KF5ConfigWidgets)
BuildRequires:  cmake(KF5DAV)
BuildRequires:  cmake(KF5DocTools)
BuildRequires:  cmake(KF5Holidays)
BuildRequires:  cmake(KF5ItemModels)
BuildRequires:  cmake(KF5KCMUtils)
BuildRequires:  cmake(KF5KDELibs4Support)
BuildRequires:  cmake(KF5KIO)
BuildRequires:  cmake(KF5Kross)
BuildRequires:  cmake(KF5NotifyConfig)
BuildRequires:  cmake(KF5TextWidgets)
BuildRequires:  cmake(KF5WindowSystem)

BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtdeclarative-devel
BuildRequires:  qt5-qtlocation-devel
BuildRequires:  qt5-qtxmlpatterns-devel
BuildRequires:  qt5-qtwebchannel-devel
BuildRequires:  qt5-qtwebengine-devel

BuildRequires:  cmake(Qt5Keychain)
BuildRequires:  cmake(Qt5NetworkAuth)

%global majmin_ver %(echo %{version} | cut -d. -f1,2)
BuildRequires:  kf5-akonadi-calendar-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-contact-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-mime-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-notes-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-server-devel >= %{majmin_ver}
BuildRequires:  kf5-grantleetheme-devel >= %{majmin_ver}
BuildRequires:  kf5-kcalendarcore-devel >= %{majmin_ver}
BuildRequires:  kf5-kcalendarutils-devel >= %{majmin_ver}
BuildRequires:  kf5-kidentitymanagement-devel >= %{majmin_ver}
BuildRequires:  kf5-kimap-devel >= %{majmin_ver}
BuildRequires:  kf5-kmailtransport-devel >= %{majmin_ver}
BuildRequires:  kf5-kmbox-devel >= %{majmin_ver}
BuildRequires:  kf5-kmime-devel >= %{majmin_ver}
BuildRequires:  kf5-kpimtextedit-devel >= %{majmin_ver}
BuildRequires:  kf5-pimcommon-devel >= %{majmin_ver}
BuildRequires:  kf5-syndication-devel >= %{majmin_ver}
BuildRequires:  libkgapi-devel >= %{majmin_ver}
BuildRequires:  kf5-libkdepim-devel >= %{majmin_ver}
BuildRequires:  kf5-kldap-devel >= %{majmin_ver}
# https://bugzilla.redhat.com/show_bug.cgi?id=1662756
Requires: libkgapi%{?_isa} >= %{majmin_ver}
BuildRequires:  cmake(KF5PimCommon)

BuildRequires:  cmake(Qca-qt5)

BuildRequires:  libkolabxml-devel >= 1.1

# needed by google calendar resource
BuildRequires:  pkgconfig(libical)
BuildRequires:  pkgconfig(libxslt) pkgconfig(libxml-2.0)

BuildRequires:  pkgconfig(shared-mime-info)

%if !0%{?bootstrap}
BuildRequires:  cmake(Qt5TextToSpeech)
%endif

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: kf5-akonadi-server-mysql >= %{majmin_ver}
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
%{summary}.


%prep
%autosetup -n kdepim-runtime-%{version}%{?pre} -p1


%build
%cmake_kf5 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%make_build -C "%{_vpath_builddir}/resources/etesync"


%install
%make_install -C "%{_vpath_builddir}/resources/etesync"


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 20" -C %{_target_platform} ||:
%endif

%files
%{_kf5_bindir}/akonadi_*
%{_kf5_datadir}/akonadi/agents/*
%{_kf5_datadir}/icons/hicolor/*/apps/*


%changelog
* Wed May 24 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 23.04.1-1
- Update to version 23.04.1
* Tue Mar 29 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 21.12.2-1
- Update to version 21.12.2
* Sun Nov 08 2020 Tom Hacohen <tom@etesync.com> 0.99-2
- Only build the akonadi resource
* Fri Nov 06 2020 Tom Hacohen <tom@etesync.com> 0.99-1
- Update to EteSync 2.0
